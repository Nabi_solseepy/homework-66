import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burger-project-js3.firebaseio.com/'
});

instance.interceptors.request.use((req) => {
    console.log('Запрос отправлен')
    return req
})

instance.interceptors.response.use(res => {
    console.log('Ответ пришел')
    return res
})


export default instance;