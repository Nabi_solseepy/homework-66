import React from 'react';

import './Spinner.css';

const Spinner = (props) => {
    return (
       <div className="div-spiner"> <div className="Spinner">Loading...</div></div>
    );
};

export default Spinner;
