import React from 'react';

import './Backdrop.css';
import {Spinner} from "reactstrap";

const Backdrop = props => (
    props.show ? <div onClick={props.onClick} className="Backdrop"><Spinner/></div> : null
);

export default Backdrop;
