import React from 'react';

const OrderItem = (props) => {

    const ingredientOutput = Object.keys(props.ingredients).map(igKey => (
        <span key={igKey}>{igKey} ({props.ingredients[igKey]})</span>
    ));
    return (
        <div>
            {ingredientOutput}
        </div>
    );
};

export default OrderItem;