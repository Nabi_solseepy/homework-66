import React, {Component} from 'react';
import { Nav, NavItem, NavLink} from "reactstrap";

class HeaderNav extends Component {
    render() {
        return (
            <div>
                <Nav tabs>
                    <NavItem>
                        <NavLink href="#" active>post</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#">add</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#">Another Link</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink disabled href="#">Disabled Link</NavLink>
                    </NavItem>
                </Nav>

            </div>
        );
    }
}

export default HeaderNav;