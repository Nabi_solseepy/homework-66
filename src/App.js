import React, { Component } from 'react';
import './App.css';
import HeaderNav from "./component/HeaderNav/HeaderNav";
import Orders from "./containers/Orders/Orders";

class App extends Component {
  render() {
    return (
      <div className="App">
       <HeaderNav/>
      < Orders/>
      </div>
    );
  }
}

export default App;
