import React, {Component, Fragment} from 'react';
import Backdrop from "../../component/Bacdrop/Backdrop";

const WithDataHandler = (WrappedComponent, axios) => {
    return class withLoading extends Component {
        constructor(props) {
            super(props);

            this.state = {
                loading: false
            };

           this.state.interceptorReq = axios.interceptors.request.use(req => {
                this.setState({loading: true});
                return req
            });

           this.state.interceptorRes = axios.interceptors.response.use(res => {
                this.setState({loading: false});
                return res
            })
        }
       componentWillUnmount() {
            axios.interceptors.eject(this.state.interceptorReq);
            axios.interceptors.eject(this.state.interceptorRes);
       }

        render() {

            return (
                <Fragment>
                    { this.state.loading && <Backdrop show={this.state.loading}/> }
                    <WrappedComponent {...this.props}/>
                </Fragment>


            )

        }
    }
};

export default WithDataHandler;