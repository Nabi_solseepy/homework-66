import React, {Component} from 'react';
import axios from '../../axios-orders'
import OrderItem from "../../component/OrderItem/OrderItem";

import withLoading from '../../HOC/withDataHandler/withDataHandler'


class Orders extends Component {
    state = {
        orders: []
    };


    componentDidMount() {
      axios.get('/orders.json').then(response => {
          console.log(response);
          const orders = Object.keys(response.data).map(key => {
              return{
                  ...response.data[key],
                  id: key
              }
          });

             this.setState({orders: orders})
      })
    }

    render() {
        return this.state.orders.map(order => (
            <OrderItem ingredients={order.ingredients}/>
        ));
    }
}

export default withLoading(Orders, axios);